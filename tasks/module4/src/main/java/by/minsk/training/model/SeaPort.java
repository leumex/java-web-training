package by.minsk.training.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SeaPort {
    private final LinkedList<Berth> availableBerths = new LinkedList<>();
    private final LinkedList<Berth> busyBerths = new LinkedList<>();
    private final int portCapacity = 4;
    private final Lock portLock = new ReentrantLock();
    private final Condition busyPort = portLock.newCondition();
    private final Logger logger = LogManager.getLogger(SeaPort.class);

    private SeaPort() {
        logger.info("Welcome to the Nikolaev International Seaport. " + portCapacity + " berths are available for coming ships.");
    }

    private static class SinglePort {
        private static final SeaPort INSTANCE = new SeaPort();
    }

    public static SeaPort getInstance() {
        return SinglePort.INSTANCE;
    }

    public Berth occupyBerth() {
        Berth freeBerth = null;
        try {
            portLock.lock();
            if (availableBerths.isEmpty() && busyBerths.size() == portCapacity) {
                try {
                    busyPort.await();
                } catch (InterruptedException e) {
                    logger.error(e.getMessage());
                }
            } else if (availableBerths.isEmpty() && busyBerths.size() < portCapacity) {
                Berth berth = new Berth();
                availableBerths.add(berth);
            } else if (availableBerths.isEmpty()) {
                throw new IllegalThreadStateException("Incorrect Thread flow");
            }
            freeBerth = availableBerths.removeFirst();
            busyBerths.add(freeBerth);
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            portLock.unlock();
        }
        return freeBerth;
    }

    public void releaseBerth(Berth berth) {
        portLock.lock();
        try {
            if (availableBerths.size() >= portCapacity) {
                throw new IllegalThreadStateException("Incorrect Thread flow");
            }
            busyBerths.remove(berth);
            availableBerths.add(berth);
            busyPort.signal();
        } catch (Exception e) {
            logger.error(e.getMessage());
        } finally {
            portLock.unlock();
        }
    }

    class Berth {
        private Ship ship;

        public void setShip(Ship ship) {
            this.ship = ship;
            ship.nextState();
        }

        void operate() {
            ship.nextState();
            if (ship.isArrivedLoaded()) {
                ship.unload(new Random().nextInt(ship.getContainersOnBoard()));
                ship.nextState();
            } else {
                ship.load();
                ship.nextState();
            }
        }
    }
}



