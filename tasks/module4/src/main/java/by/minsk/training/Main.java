package by.minsk.training;

import by.minsk.training.service.Demonstration;

public class Main {

    public static void main(String[] args) {
        new Demonstration().demonstrate();
    }
}
