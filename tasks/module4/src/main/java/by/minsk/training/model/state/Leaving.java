package by.minsk.training.model.state;

import by.minsk.training.model.Ship;

public class Leaving extends ShipState {

    @Override
    public void next(Ship ship) {
    }

    @Override
    public void prev(Ship ship) {
        ship.setState(new Operation());
    }
}
