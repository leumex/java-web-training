package by.minsk.training.model.state;

import by.minsk.training.model.Ship;

public abstract class ShipState {


    public abstract void next(Ship ship);
    public abstract void prev(Ship ship);


}
