package by.minsk.training.model.state;

import by.minsk.training.model.Ship;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Arrival extends ShipState {
    private static final Logger logger = LogManager.getLogger(Arrival.class);

    @Override
    public void next(Ship ship) {
        logger.info("Ship "+ship.getId()+" has been called to berth");
        ship.setState(new Mooring());

    }

    @Override
    public void prev(Ship ship) {

    }
}
