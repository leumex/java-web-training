package by.minsk.training.model;

import by.minsk.training.model.state.Arrival;
import by.minsk.training.model.state.ShipState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Ship implements Runnable {
    private static int count = 0;
    private ShipState state;
    private int containerCapacity;
    private boolean arrivedLoaded;
    private static final Logger LOGGER = LogManager.getLogger(Ship.class);
    private int containersOnBoard;
    private int id;


    public Ship() {
        this.state = new Arrival();
        id = count++;
        this.containerCapacity = new Random().nextInt(20000);
        this.arrivedLoaded = new Random().nextBoolean();
        if (this.arrivedLoaded) {
            this.containersOnBoard = new Random().nextInt(this.containerCapacity);
        }
    }


    public void load() {
        LOGGER.info("Ship " + id + ": loading in progress..");
        while (this.containerCapacity != this.containersOnBoard) {
            try {
                TimeUnit.NANOSECONDS.sleep(1);
            } catch (InterruptedException e) {
                LOGGER.error(e.getMessage());
            }
            containersOnBoard++;
        }
    }

    public void unload(int n) {
        LOGGER.info("Ship " + id + ": unloading in progress..");
        while (this.containersOnBoard - n > 0) {
            try {
                TimeUnit.NANOSECONDS.sleep(1);
            } catch (InterruptedException e) {
                LOGGER.error(e.getMessage());
            }
            containersOnBoard--;
        }
    }

    public boolean isArrivedLoaded() {
        return arrivedLoaded;
    }

    public int getId() {
        return this.id;
    }

    public void nextState() {
        state.next(this);
    }

    public void previousState() {
        state.prev(this);
    }

    public void setState(ShipState state) {
        this.state = state;
    }

    public ShipState getShipState() {
        return this.state;
    }

    public int getContainersOnBoard() {
        return this.containersOnBoard;
    }

    @Override
    public void run() {
        SeaPort port = SeaPort.getInstance();
        LOGGER.info("Ship " + id + " has arrived at port, " +
                (this.arrivedLoaded ? ("loaded with " + this.containersOnBoard + " containers") : ("to be loaded with containers")));
        SeaPort.Berth berth = port.occupyBerth();
        berth.setShip(this);
        try {
            TimeUnit.MILLISECONDS.sleep(100);
        } catch (InterruptedException e) {
            LOGGER.info(e.getMessage());
        }
        berth.operate();
        port.releaseBerth(berth);
    }
}
