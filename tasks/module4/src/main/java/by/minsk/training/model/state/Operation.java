package by.minsk.training.model.state;

import by.minsk.training.model.Ship;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Operation extends ShipState {

    private static final Logger LOGGER = LogManager.getLogger(Operation.class);

    @Override
    public void next(Ship ship) {
        LOGGER.info("Ship "+ ship.getId()+" has abandonded the berth after completed loading/unloading");
        ship.setState(new Leaving());
    }

    @Override
    public void prev(Ship ship) {
ship.setState(new Mooring());
    }
}
