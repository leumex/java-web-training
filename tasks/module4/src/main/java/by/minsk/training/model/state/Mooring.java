package by.minsk.training.model.state;

import by.minsk.training.model.Ship;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Mooring extends ShipState {

    private static final Logger LOGGER = LogManager.getLogger(Mooring.class);


    @Override
    public void next(Ship ship) {
        LOGGER.info("Ship "+ ship.getId()+" is being unloaded or loaded");
        ship.setState(new Operation());
    }

    @Override
    public void prev(Ship ship) {

    }
}
