package by.minsk.training.service;

import by.minsk.training.model.Candy;
import by.minsk.training.repository.Repository;
import by.minsk.training.repository.RepositoryImpl;

import java.util.List;

public class CandiesService {
    private Repository<Candy> repository = new RepositoryImpl();

    public long save(Candy entity){
        return repository.create(entity);
    }

    public Candy read(long id){
        return repository.read(id);
    }

    public List<Candy> getAll (){
        return repository.getAll();
    }

    public void clear(){
        repository.deleteAll();
    }
}
