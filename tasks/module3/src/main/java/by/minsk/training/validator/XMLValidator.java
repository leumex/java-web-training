package by.minsk.training.validator;

public interface XMLValidator {
    boolean isValid (String file1);
}
