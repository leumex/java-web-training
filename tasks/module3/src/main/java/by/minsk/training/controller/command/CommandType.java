package by.minsk.training.controller.command;

public enum CommandType {
DOM,SAX,StAX,DEFAULT;

    public static boolean belong(String s){
        for (CommandType value : values()) {
            if (!(value.toString().equalsIgnoreCase(s)))
                    return false;
        }
        return true;
    }
}
