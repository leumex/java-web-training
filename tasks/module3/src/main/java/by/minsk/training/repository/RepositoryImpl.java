package by.minsk.training.repository;

import by.minsk.training.model.Candy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RepositoryImpl implements Repository<Candy> {

    Map<Long, Candy> sweeties = new HashMap<>();
    private static long count = 0;

    @Override
    public long create(Candy candy) {
        sweeties.put(count, candy);
        return count++;
    }

    @Override
    public boolean update(Candy candy) {
        sweeties.put(candy.getId(), candy);
        return true;
    }

    @Override
    public Candy read(long id) {
        return sweeties.get(id);
    }

    @Override
    public List<Candy> getAll() {
        return new ArrayList<>(sweeties.values());
    }

    @Override
    public void deleteAll(){
        sweeties.clear();
    }
}
