package by.minsk.training.model;

public enum ChocolateType {
    MILK, BLACK, BITTER, WHITE, NOT_USED
}
