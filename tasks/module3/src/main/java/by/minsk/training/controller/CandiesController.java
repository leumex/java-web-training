package by.minsk.training.controller;

import by.minsk.training.controller.command.Command;
import by.minsk.training.controller.command.CommandProvider;
import by.minsk.training.model.Candy;
import by.minsk.training.parser.CandiesParseException;
import by.minsk.training.service.CandiesService;
import by.minsk.training.validator.XMLValidator;

import java.util.List;

public class CandiesController {
    private CandiesService service;
    private XMLValidator validator;
    private CommandProvider commandProvider;

    public CandiesController(CandiesService service, XMLValidator validator, CommandProvider commandProvider) {
        this.service = service;
        this.validator = validator;
        this.commandProvider = commandProvider;
    }

    void upload(String filePath, String commandType) {
        try {
            Command command = commandProvider.getCommand(commandType);
            if (validator.isValid(filePath)) {
                List<Candy> list = command.parse(filePath);
                list.forEach(service::save);
                System.out.println(list);
            }
        } catch (CandiesParseException e) {
            //Log
            e.printStackTrace();
        }
    }
}
