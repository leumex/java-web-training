package by.minsk.training.parser;

import by.minsk.training.model.Candy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class SAXCustomParser implements Parser {


    private static final Logger LOGGER = LogManager.getLogger(SAXCustomParser.class);
    @Override
    public List<Candy> parse(String filepath) throws CandiesParseException {
        List<Candy> list;
        try {
            LOGGER.debug("SAX parsing has started...");
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            XMLCandyHandler handler = new XMLCandyHandler();
            parser.parse(new File(filepath), handler);
            LOGGER.debug("SAX parsing has successfully finished, "+handler.getCandies().size()+" have been found");
            list = handler.getCandies();
        } catch (ParserConfigurationException | SAXException | IOException e) {
            Class clazz = e.getClass();
            String errorMessage = clazz == ParserConfigurationException.class ?
                    "DOM DocumentBuilder cannot be created..." :
                    clazz == SAXException.class ? "Errors appeared while parsing the file!" :
                            clazz == IOException.class ? "IO process is interrupted!" : "unknown issue..";
            LOGGER.error("new CandiesParseException has just been thrown: "+errorMessage);
            throw new CandiesParseException(errorMessage);
        }
        return list;
    }
}
