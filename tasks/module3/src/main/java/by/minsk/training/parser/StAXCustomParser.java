package by.minsk.training.parser;

import by.minsk.training.model.Candy;
import by.minsk.training.model.ChocolateType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

public class StAXCustomParser implements Parser {

    private static final Logger LOGGER = LogManager.getLogger(StAXCustomParser.class);
    @Override
    public List<Candy> parse(String filePath) throws CandiesParseException {
        List<Candy> list = new ArrayList<>();
        try {
            XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
            XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(filePath));
            Candy.Builder builder = null;
            Map<String, Double> ingredients = new HashMap<>();
            while (reader.hasNext()) {
                XMLEvent xmlEvent = reader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    if (startElement.getName().getLocalPart().equalsIgnoreCase("candy")) {
                        builder = new Candy.Builder();
                        Attribute typeAttr = startElement.getAttributeByName(new QName("type"));
                        if (typeAttr != null) {
                            builder.setType(typeAttr.getValue());
                        }
                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("Name")) {
                        xmlEvent = reader.nextEvent();
                        builder.setName(xmlEvent.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("Ingredients")) {
                        ingredients = new HashMap<>();
                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("Energy")) {
                        Attribute attr = startElement.getAttributeByName(new QName("kcal"));
                        if (attr != null) {
                            builder.setEnergy(Double.parseDouble(attr.getValue()));
                        }
                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("Water")) {
                        Attribute attr = startElement.getAttributeByName(new QName("mg"));
                        if (attr != null) {
                            ingredients.put("water", Double.parseDouble(attr.getValue()));
                        }
                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("Sugar")) {
                        Attribute attr = startElement.getAttributeByName(new QName("mg"));
                        if (attr != null) {
                            ingredients.put("sugar", Double.parseDouble(attr.getValue()));
                        }
                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("Fructose")) {
                        Attribute attr = startElement.getAttributeByName(new QName("mg"));
                        if (attr != null) {
                            ingredients.put("fructose", Double.parseDouble(attr.getValue()));
                        }
                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("Vanillin")) {
                        Attribute attr = startElement.getAttributeByName(new QName("mg"));
                        if (attr != null) {
                            ingredients.put("vanillin", Double.parseDouble(attr.getValue()));
                        }
                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("Value")) {
                        Map<String, Double> values = new HashMap<>();
                        Iterator<Attribute> attributes = startElement.getAttributes();
                        while (attributes.hasNext()) {
                            Attribute attribute = attributes.next();
                            values.put(attribute.getName().getLocalPart(), Double.parseDouble(attribute.getValue()));
                        }
                        builder.setValues(values);
                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("Production")) {
                        xmlEvent = reader.nextEvent();
                        builder.setProducer(xmlEvent.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equalsIgnoreCase("Chocolate")) {
                        xmlEvent = reader.nextEvent();
                        builder.setChocolateType(ChocolateType.valueOf(xmlEvent.asCharacters().getData().toUpperCase()));
                    }
                }
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("candy")) {
                        builder.setIngredients(ingredients);
                        LOGGER.debug(builder.build().getName()+ " candy has been created");
                        list.add(builder.build());
                    }
                }
            }
        } catch (XMLStreamException | IOException e) {
            Class clazz = e.getClass();
            String errorMessage = clazz == ParserConfigurationException.class ?
                    "DOM DocumentBuilder cannot be created..." :
                    clazz == XMLStreamException.class ? "Either well-formedness errors or unexpected processing " +
                            "conditions occured!" :
                            clazz == IOException.class ? "IO process is interrupted!" : "unknown issue..";
            LOGGER.error("new CandiesParseException has just been thrown: "+errorMessage);
            throw new CandiesParseException(errorMessage);
        }
        return list;
    }
}
