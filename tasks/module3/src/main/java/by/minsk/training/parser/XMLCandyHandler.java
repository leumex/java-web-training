package by.minsk.training.parser;

import by.minsk.training.model.Candy;
import by.minsk.training.model.ChocolateType;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XMLCandyHandler extends DefaultHandler {

    private boolean productionIndicator;
    private boolean nameIndicator;
    private boolean chocolateIndicator;

    private Candy.Builder builder;
    private Map<String, Double> ingredients;
    private List<Candy> candies = new ArrayList<>();

    public List<Candy> getCandies() {
        return candies;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("candy")) {
            builder = new Candy.Builder();
            builder.setType(attributes.getValue("type"));
        }
        if (qName.equalsIgnoreCase("Value")) {
            Map<String, Double> values = new HashMap<>();
            for (int i = 0; i < attributes.getLength(); i++) {
                values.put(attributes.getQName(i), Double.parseDouble(attributes.getValue(i)));
            }
            builder.setValues(values);
        }
        if (qName.equalsIgnoreCase("Production")) {
            productionIndicator = true;
        }
        if (qName.equalsIgnoreCase("Name")) {
            nameIndicator = true;
        }
        if (qName.equalsIgnoreCase("Energy")) {
            builder.setEnergy(Double.parseDouble(attributes.getValue("kcal")));
        }
        if (qName.equalsIgnoreCase("Ingredients")) {
            ingredients = new HashMap<>();
        }
        if (qName.equalsIgnoreCase("Water")) {
            ingredients.put("water", Double.parseDouble(attributes.getValue("mg")));
        }
        if (qName.equalsIgnoreCase("Sugar")) {
            ingredients.put("sugar", Double.parseDouble(attributes.getValue("mg")));
        }
        if (qName.equalsIgnoreCase("Fructose")) {
            ingredients.put("fructose", Double.parseDouble(attributes.getValue("mg")));
        }
        if (qName.equalsIgnoreCase("Vanillin")) {
            ingredients.put("vanillin", Double.parseDouble(attributes.getValue("mg")));
        }
        if (qName.equalsIgnoreCase("Chocolate")) {
            chocolateIndicator = true;
        }

    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("candy")) {
            builder.setIngredients(ingredients);
            candies.add(builder.build());
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (nameIndicator) {
            builder.setName(new String(ch, start, length));
            nameIndicator = false;
        }
        if (productionIndicator) {
            builder.setProducer(new String(ch, start, length));
            productionIndicator = false;
        }
        if (chocolateIndicator) {
            builder.setChocolateType(ChocolateType.valueOf(new String(ch, start, length).toUpperCase()));
            chocolateIndicator = false;
        }

    }
}
