package by.minsk.training.parser;

import by.minsk.training.model.Candy;

import java.util.List;

public interface Parser {

    List<Candy> parse(String filePath)throws CandiesParseException;
}
