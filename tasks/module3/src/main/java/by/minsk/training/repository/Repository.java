package by.minsk.training.repository;

import java.util.List;

public interface Repository<T> {

    long create (T entity);
    boolean update (T entity);
    T read (long id);
    List<T> getAll();
    void deleteAll();

}
