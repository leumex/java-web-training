package by.minsk.training.parser;

public class CandiesParseException extends Exception {
    public CandiesParseException(String errorMessage){
        super(errorMessage);
    }
}
