package by.minsk.training.model;

import java.util.Map;
import java.util.Objects;

public class Candy {

    private long id;
    private String name;
    private String type;
    private String producer;
    private double energy;
    private ChocolateType chocoType = ChocolateType.NOT_USED;
    private Map<String, Double> ingredients;
    private Map<String, Double> values;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public double getEnergy() {
        return energy;
    }

    public void setEnergy(double energy) {
        this.energy = energy;
    }

    public Map<String, Double> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Map<String, Double> ingredients) {
        this.ingredients = ingredients;
    }

    public Map<String, Double> getValues() {
        return values;
    }

    public void setValues(Map<String, Double> values) {
        this.values = values;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Candy candy = (Candy) o;
        return Double.compare(candy.energy, energy) == 0 &&
                Objects.equals(name, candy.name) &&
                Objects.equals(type, candy.type) &&
                Objects.equals(producer, candy.producer) &&
                Objects.equals(ingredients, candy.ingredients) &&
                Objects.equals(values, candy.values);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type, producer, energy, ingredients, values);
    }

    public static class Builder {
        private Candy newCandy;

        public Builder() {
            newCandy = new Candy();
        }

        public Builder setName(String name) {
            newCandy.name = name;
            return this;
        }

        public Builder setProducer(String producer) {
            newCandy.producer = producer;
            return this;
        }

        public Builder setType(String type) {
            newCandy.type = type;
            return this;
        }

        public Builder setEnergy(double energy) {
            newCandy.energy = energy;
            return this;
        }

        public Builder setChocolateType(ChocolateType type) {
            newCandy.chocoType = type;
            return this;
        }

        public Builder setIngredients(Map<String, Double> ingredients) {
            newCandy.ingredients = ingredients;
            return this;
        }

        public Builder setValues (Map<String, Double> values){
            newCandy.values = values;
            return this;
        }

        public Candy build(){
            return newCandy;
        }
    }
}
