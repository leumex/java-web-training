package by.minsk.training.controller.command;

import by.minsk.training.model.Candy;
import by.minsk.training.parser.CandiesParseException;
import by.minsk.training.parser.Parser;

import java.util.List;

public interface Command {
    List<Candy> parse(String filePath) throws CandiesParseException;
}
