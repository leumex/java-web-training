package by.minsk.training.controller.command;

import java.util.EnumMap;
import java.util.Map;

public class CommandProvider {

    private Map<CommandType, Command> commands = new EnumMap<>(CommandType.class);

    {
        commands.put(CommandType.DEFAULT, new DefaultCommand());
        commands.put(CommandType.DOM, new DomCommand());
        commands.put(CommandType.SAX, new SAXCommand());
        commands.put(CommandType.StAX, new StAXCommand());
    }

    public Command getCommand(String type){
        return (CommandType.belong(type)?commands.get(CommandType.valueOf(type)):commands.get(CommandType.DEFAULT));
    }

    public void addCommand(CommandType type, Command command){
        commands.put(type,command);
    }
}
