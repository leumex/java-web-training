package by.minsk.training.validator;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class XMLValidatorImpl implements XMLValidator {

    private final String xsdFile = this.getClass().getClassLoader()
            .getResource("candies.xsd").getFile();

    @Override
    public boolean isValid(String xmlFile) {
        try (InputStream xml = new FileInputStream(new File(xmlFile));
             InputStream xsd = new FileInputStream(new File(xsdFile))) {
            SchemaFactory factory =
                    SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new StreamSource(xsd));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(xml));
            return true;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }
}
