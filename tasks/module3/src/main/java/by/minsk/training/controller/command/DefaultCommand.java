package by.minsk.training.controller.command;

import by.minsk.training.model.Candy;

import java.util.Collections;
import java.util.List;

public class DefaultCommand implements Command {

    @Override
    public List<Candy> parse (String filePath){
        return Collections.emptyList();
    }

}
