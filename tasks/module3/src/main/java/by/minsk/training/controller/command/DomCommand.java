package by.minsk.training.controller.command;

import by.minsk.training.model.Candy;
import by.minsk.training.parser.CandiesParseException;
import by.minsk.training.parser.DOMCustomParser;

import java.util.List;

public class DomCommand implements Command {

    @Override
    public List<Candy> parse(String filePath) throws CandiesParseException {
        return new DOMCustomParser().parse(filePath);
    }
}
