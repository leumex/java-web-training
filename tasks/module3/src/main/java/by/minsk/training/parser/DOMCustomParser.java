package by.minsk.training.parser;

import by.minsk.training.model.Candy;
import by.minsk.training.model.ChocolateType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DOMCustomParser implements Parser {

    private static final Logger LOGGER = LogManager.getLogger(DOMCustomParser.class);

    @Override
    public List<Candy> parse(String filePath) throws CandiesParseException {
        List<Candy> list = new ArrayList<>();
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            File file = new File(filePath);
            builder.parse(file);
            Document document = builder.parse(file);
            Element root = document.getDocumentElement();
            NodeList children = root.getElementsByTagName("candy");
            for (int i = 0; i < children.getLength(); i++) {
                Element child = (Element) children.item(i);
                Candy.Builder candyBuilder = new Candy.Builder();
                candyBuilder.setType(child.getAttribute("type"));
                NodeList babies = child.getChildNodes();
                for (int j = 0; j < babies.getLength(); j++) {
                    if (babies.item(j) instanceof Element) {
                        Element baby = (Element) babies.item(j);
                        switch (baby.getTagName()) {
                            case "Production":
                                candyBuilder.setProducer(baby.getTextContent().replace("\n", "").trim());
                                break;
                            case "Name":
                                candyBuilder.setName(baby.getTextContent().replace("\n", "").trim());
                                break;
                            case "Energy":
                                candyBuilder.setEnergy(Double.parseDouble(baby.getAttribute("kcal")));
                                break;
                            case "Ingredients":
                                Map<String, Double> ingredients = new HashMap<>();
                                NodeList parts = baby.getChildNodes();
                                for (int k = 0; k < parts.getLength(); k++) {
                                    if (parts.item(k) instanceof Element) {
                                        Element part = (Element) parts.item(k);
                                        switch (part.getTagName()) {
                                            case "Chocolate":
                                                String choco = part.getTextContent().replace("\n", "").trim();
                                                LOGGER.debug("chocolate detected "+choco+"!");
                                                candyBuilder.setChocolateType(ChocolateType.valueOf(choco.toUpperCase()));
                                                break;
                                            case "Vanillin":
                                                ingredients.put("vanillin", Double.parseDouble(part.getAttribute("mg")));
                                                break;
                                            case "Fructose":
                                                ingredients.put("fructose", Double.parseDouble(part.getAttribute("mg")));
                                                break;
                                            case "Sugar":
                                                ingredients.put("sugar", Double.parseDouble(part.getAttribute("mg")));
                                                break;
                                            case "Water":
                                                ingredients.put("water", Double.parseDouble(part.getAttribute("mg")));
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }
                                candyBuilder.setIngredients(ingredients);
                                break;
                            case "Value":
                                candyBuilder.setValues(this.collectInMap(baby));
                                break;
                            default:
                                break;
                        }
                    }
                }
                LOGGER.debug("Candy object created at iteration"+i);
                list.add(candyBuilder.build());
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            Class clazz = e.getClass();
            String errorMessage = clazz == ParserConfigurationException.class ?
                    "DOM DocumentBuilder cannot be created..." :
                    clazz == SAXException.class ? "Errors appeared while parsing the file!" :
                            clazz == IOException.class ? "IO process is interrupted!" : "unknown issue..";
            LOGGER.error("Mistake occured ("+errorMessage+"), but it has been thrown into upper level");
            throw new CandiesParseException(errorMessage);
        }
        return list;
    }

    private Map<String, Double> collectInMap(Element element) {
        Map<String, Double> result = new HashMap<>();
        if (element.hasAttributes()) {
            NamedNodeMap attributes = element.getAttributes();
            for (int i = 0; i < attributes.getLength(); i++) {
                result.put(attributes.item(i).getNodeName(), Double.valueOf(attributes.item(i).getNodeValue()));
            }
        }
        return result;
    }
}
