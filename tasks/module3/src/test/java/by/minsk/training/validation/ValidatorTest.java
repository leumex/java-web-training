package by.minsk.training.validation;

import by.minsk.training.validator.XMLValidator;
import by.minsk.training.validator.XMLValidatorImpl;
import org.junit.Assert;
import org.junit.Test;

public class ValidatorTest {

    @Test
    public void shouldValidate(){
        XMLValidator validator = new XMLValidatorImpl();
        Assert.assertTrue(validator.isValid(this.getClass().getClassLoader().getResource("candies.xml").getFile()));
    }

    @Test
    public void shouldNotValidate(){
        XMLValidator validator = new XMLValidatorImpl();
        Assert.assertFalse(validator.isValid(this.getClass().getClassLoader().getResource("candies1.xml").getFile()));
    }
}

