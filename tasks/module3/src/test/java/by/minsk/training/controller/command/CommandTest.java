package by.minsk.training.controller.command;

import by.minsk.training.model.Candy;
import by.minsk.training.parser.CandiesParseException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CommandTest {

    private static final Logger LOGGER = LogManager.getLogger(CommandTest.class);
    private CommandProvider provider = new CommandProvider();
    private String filePath = this.getClass().getClassLoader().getResource("candies.xml").getFile();

    @Test
    public void shouldProduceEmptyList() {
        Command command = provider.getCommand("ha ha");
        LOGGER.info("Default command test running...");
        try {
            List<Candy> list = command.parse(filePath);
            Assert.assertTrue(Collections.emptyList().equals(list));
        } catch (CandiesParseException e) {
            LOGGER.log(Level.ERROR, e.getMessage());
        }
    }

    @Test
    public void revisedDefaultCommandShouldWork() {
        provider.addCommand(CommandType.DEFAULT, (String file_Path) -> {List<Candy> testList = new ArrayList<>();
        Candy.Builder builder = new Candy.Builder();
        builder.setName("lollipop");
        testList.add(builder.build());
        return testList;
        
        });
        try {
            Assert.assertTrue(provider.getCommand("default").parse(filePath).size()==1);
        } catch (CandiesParseException e) {
            LOGGER.error(e.getMessage());
        }
    }
}

