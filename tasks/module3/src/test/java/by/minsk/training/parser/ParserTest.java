package by.minsk.training.parser;

import by.minsk.training.model.Candy;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ParserTest {

    @Test
    public void shouldCreateEntity() {
        Parser parser = new DOMCustomParser();
        List<Candy> receivedList = new ArrayList<>();
        try {
            receivedList = parser.parse(this.getClass().getClassLoader().getResource("candies.xml").getFile());
        } catch (CandiesParseException ex) {
        }
        Candy candy = receivedList.get(2);
        Assert.assertEquals(candy.getName(), "Berezka");
        Assert.assertTrue(candy.getEnergy() == Double.parseDouble("35.5"));
    }

    @Test
    public void shouldParse() {
        Parser parser = new SAXCustomParser();
        List<Candy> receivedList = new ArrayList<>();
        try {
            receivedList = parser.parse(this.getClass().getClassLoader().getResource("candies.xml").getFile());
        } catch (CandiesParseException ex) {
        }
        Assert.assertTrue(receivedList.size() == 16);
    }

    @Test
    public void shouldBeTheSame() {
        Parser parser1 = new StAXCustomParser();
        Parser parser2 = new SAXCustomParser();
        Parser parser3 = new DOMCustomParser();
        List<Candy> receivedList1 = new ArrayList<>();
        List<Candy> receivedList2 = new ArrayList<>();
        List<Candy> receivedList3 = new ArrayList<>();
        try {
            receivedList1 = parser1.parse(this.getClass().getClassLoader().getResource("candies.xml").getFile());
            receivedList2 = parser2.parse(this.getClass().getClassLoader().getResource("candies.xml").getFile());
            receivedList3 = parser3.parse(this.getClass().getClassLoader().getResource("candies.xml").getFile());
        } catch (CandiesParseException e) {
        }
        Assert.assertNotNull(receivedList1.get(15));
        Assert.assertEquals(receivedList1, receivedList2);
        Assert.assertEquals(receivedList1, receivedList3);
    }
}
