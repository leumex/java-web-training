package by.minsk.training.model;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Sentence implements CompositeElement {
    private List<CompositeElement> words = new LinkedList<>();

    @Override
    public String extract() {
        return words.stream().map(LeafElement::extract).collect(Collectors.joining());
    }

    @Override
    public List<CompositeElement> getAll() {
        return words;
    }

    @Override
    public void addElement(CompositeElement element) {
        words.add(element);
    }
}
