package by.minsk.training.model;

import java.util.List;
import java.util.stream.Collectors;

public class Text implements CompositeElement {

    private List<CompositeElement> paragraphs;

    @Override
    public String extract() {
        return paragraphs.stream().map(LeafElement::extract).collect(Collectors.joining());
    }

    @Override
    public List<CompositeElement> getAll() {
        return paragraphs;
    }

    @Override
    public void addElement(CompositeElement element) {
        paragraphs.add(element);
    }
}
