package by.minsk.training.repository;

import java.util.List;

public interface Repository<T> {
    long create(T entity);
    void update(List<T> list);
    T read (long id);
    List<T> getAll();
    void remove(long id);
}
