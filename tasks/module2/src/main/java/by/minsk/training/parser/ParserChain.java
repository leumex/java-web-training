package by.minsk.training.parser;

import java.util.List;

public interface ParserChain<T> {
    List<T> parseLine(String line);
    ParserChain<T> linkWith(ParserChain<T> next);
}
