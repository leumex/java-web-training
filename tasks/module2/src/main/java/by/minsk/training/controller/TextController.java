package by.minsk.training.controller;

import by.minsk.training.model.CompositeElement;
import by.minsk.training.parser.SimpleParser;
import by.minsk.training.service.TextService;

import java.util.List;
import java.util.stream.Collectors;

public class TextController {

    private SimpleParser parser;
    private TextService service;
    private TextSource source;

    public TextController(SimpleParser parser, TextService service, TextSource source) {
        this.parser = parser;
        this.service = service;
        this.source = source;
    }

    public void store(String text) {
        parser.parseLine(text).forEach(service::save);
    }

    public void sortBySentenceQuantity() {
        List<CompositeElement> list = service.getAll()
                .stream()
                .sorted((s1, s2) -> Integer.compare(s1.getAll().size(), s2.getAll().size()))
                .collect(Collectors.toList());
        service.reset(list);
    }

    public String extract() {
        return service.getAll()
                .stream()
                .map(CompositeElement::extract)
                .collect(Collectors.joining(" "));
    }
}

