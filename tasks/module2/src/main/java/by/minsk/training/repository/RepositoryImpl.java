package by.minsk.training.repository;

import by.minsk.training.model.CompositeElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RepositoryImpl implements Repository<CompositeElement> {
    private Map<Long, CompositeElement> map = new HashMap();
    private static long count = 0;

    @Override
    public long create(CompositeElement entity){
        long index = count++;
        map.put(index, entity);
        return index;
    }


    public void update(List<CompositeElement> entities){
        map.clear();
        count = 0;
        entities.forEach(this::create);
    }

    @Override
    public CompositeElement read (long id){
        return map.get(id);
    }

    @Override
    public List<CompositeElement> getAll(){
        return new ArrayList<>(map.values());
    }

    @Override
    public void remove(long id){
        map.remove(id);
    }
}
