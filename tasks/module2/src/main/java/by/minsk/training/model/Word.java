package by.minsk.training.model;

import java.util.Collections;
import java.util.List;

public class Word implements CompositeElement {

    private String content;

    public Word(String content){
        this.content = content;
    }

    @Override
    public String extract() {
        return content + " ";
    }

    @Override
    public void addElement(CompositeElement element) {


    }

    @Override
    public List<CompositeElement> getAll() {
        return Collections.emptyList();
    }

}
