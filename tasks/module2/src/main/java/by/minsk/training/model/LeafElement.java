package by.minsk.training.model;

public interface LeafElement {
    String extract();
}
