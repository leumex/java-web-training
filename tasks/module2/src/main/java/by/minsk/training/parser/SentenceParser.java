package by.minsk.training.parser;

import by.minsk.training.model.CompositeElement;
import by.minsk.training.model.Sentence;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenceParser extends SimpleParser {
    private final String SENTENCE_TEMPLATE = "(\\n)?[A-Z].+?[.?!]+[^\\nA-Z]*";

    @Override
    public List<CompositeElement> parseLine(String line) {
        Pattern pattern = Pattern.compile(SENTENCE_TEMPLATE);
        Matcher matcher = pattern.matcher(line);
        List<CompositeElement> foundSentences = new LinkedList<>();
        while (matcher.find()) {
            Sentence sentence = new Sentence();
            nextParse(matcher.group()).forEach(sentence::addElement);
            foundSentences.add(sentence);
        }
        return foundSentences;
    }
}
