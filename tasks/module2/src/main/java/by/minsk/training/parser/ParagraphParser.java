package by.minsk.training.parser;

import by.minsk.training.model.CompositeElement;
import by.minsk.training.model.Paragraph;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParagraphParser extends SimpleParser {

    private final String PARAGRAPH_TEMPLATE = "\\t[^\\t]+[\\.\\?\\!]+";

    @Override
    public List<CompositeElement> parseLine(String line) {
        List<CompositeElement> foundParagraphs = new LinkedList<>();
        Pattern pattern = Pattern.compile(PARAGRAPH_TEMPLATE);
        Matcher matcher = pattern.matcher(line);
        while (matcher.find()) {
            Paragraph paragraph = new Paragraph();
            nextParse(matcher.group()).forEach(paragraph::addElement);
            foundParagraphs.add(paragraph);
        }
        return foundParagraphs;
    }
}
