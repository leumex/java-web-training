package by.minsk.training.parser;

import by.minsk.training.model.CompositeElement;

import java.util.Collections;
import java.util.List;

public abstract class SimpleParser implements ParserChain<CompositeElement> {
    private ParserChain<CompositeElement> next;

    public ParserChain<CompositeElement> linkWith(ParserChain<CompositeElement> next) {
        this.next = next;
        return next;
    }

    List<CompositeElement> nextParse(String line) {
        return next == null ? Collections.emptyList() : next.parseLine(line);
    }
}
