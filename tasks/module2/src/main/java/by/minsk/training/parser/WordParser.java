package by.minsk.training.parser;

import by.minsk.training.model.CompositeElement;
import by.minsk.training.model.Word;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordParser extends SimpleParser {
    private final String WORD_TEMPLATE = "([\\t\\'\\(]+)?(\\w+|\\-)([\\.\\,\\'\\)\\:\\n]+)?";

    @Override
    public List<CompositeElement> parseLine(String line) {
        Pattern pattern = Pattern.compile(WORD_TEMPLATE);
        Matcher matcher = pattern.matcher(line);
        List<CompositeElement> foundWords = new LinkedList<>();
        while (matcher.find()) {
            Word word = new Word(matcher.group());
            foundWords.add(word);
        }
        return foundWords;
    }
}
