package by.minsk.training.controller;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TextSource {
    private static final Logger LOGGER = Logger.getLogger(TextSource.class);

    public String read(String path) {
        String result = "";
        try (Stream<String> lines = Files.lines(Paths.get(path))) {
            result = lines.collect(Collectors.joining());
        } catch (IOException exception) {
            LOGGER.error("Issue with reading file " + path, exception);
        }
        return result;
    }

}
