package by.minsk.training.model;

import java.util.List;

public interface CompositeElement extends LeafElement {
    void addElement(CompositeElement element);
    List<CompositeElement> getAll();

}
