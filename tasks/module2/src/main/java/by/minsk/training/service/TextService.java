package by.minsk.training.service;

import by.minsk.training.model.CompositeElement;
import by.minsk.training.repository.Repository;

import java.util.List;

public class TextService {

    private Repository<CompositeElement> repository;

    public TextService(Repository<CompositeElement> repository) {
        this.repository = repository;
    }

    public Long save(CompositeElement element){
        return repository.create(element);
    }

    public List<CompositeElement> getAll (){
        return repository.getAll();
    }

    public void remove(Long id){
        repository.remove(id);
    }

    public CompositeElement read(Long id){
        return repository.read(id);
    }

    public void reset(List<CompositeElement> list){
        repository.update(list);
    }

}
