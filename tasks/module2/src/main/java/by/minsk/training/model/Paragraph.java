package by.minsk.training.model;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Paragraph implements CompositeElement {

        private List<CompositeElement> sentences = new LinkedList<>();



        @Override
        public String extract() {
            return sentences.stream().map(LeafElement::extract).collect(Collectors.joining());
        }

        @Override
        public List<CompositeElement> getAll() {
            return sentences;
        }

        @Override
        public void addElement(CompositeElement element) {
            sentences.add(element);
        }
}
