package by.minsk.training;

import by.minsk.training.controller.TextController;
import by.minsk.training.controller.TextSource;
import by.minsk.training.parser.ParagraphParser;
import by.minsk.training.parser.SentenceParser;
import by.minsk.training.parser.SimpleParser;
import by.minsk.training.parser.WordParser;
import by.minsk.training.repository.RepositoryImpl;
import by.minsk.training.service.TextService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

public class IntegrationTest {

    private String content;
    private TextController controller;

    @Before
    public void init() {
        TextService service = new TextService(new RepositoryImpl());
        SimpleParser parser = new ParagraphParser();
        TextSource source = new TextSource();
        parser.linkWith(new SentenceParser()).linkWith(new WordParser());
        controller = new TextController(parser, service, source);
        content = source.read(new File(this.getClass().getClassLoader()
                .getResource("text.txt").getFile()).getAbsolutePath());
    }

    @Test
    public void shouldFillRepo() {
        controller.store(content);
        String extractedContent = controller.extract();
        System.out.println(extractedContent);
        Assert.assertTrue(extractedContent.length() > 600);
    }
    @Test
    public void shouldSort() {
        controller.store(content);
        String before = controller.extract();
        System.out.println(before);
        controller.sortBySentenceQuantity();
        String after = controller.extract();
        System.out.println(after);
        Assert.assertNotEquals(before, after);
    }
}
