package by.minsk.training.repository;

import by.minsk.training.controller.TextSource;
import by.minsk.training.model.CompositeElement;
import by.minsk.training.parser.ParagraphParser;
import by.minsk.training.parser.ParserChain;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.List;

public class RepositoryTest {

    private String content;

    @Before
    public void init() {
        content = new TextSource().read(new File(this.getClass().getClassLoader()
                .getResource("text.txt").getFile()).getAbsolutePath());
    }
    @Test
    public void shouldCreateMapEntries(){
        ParserChain<CompositeElement> parser = new ParagraphParser();
        Repository<CompositeElement> repository = new RepositoryImpl();
        parser.parseLine(content).forEach(repository::create);
        Assert.assertNotNull(repository.getAll());
    }


    @Test
    public void shouldRead(){
        ParserChain<CompositeElement> parser = new ParagraphParser();
        Repository<CompositeElement> repository = new RepositoryImpl();
        List<CompositeElement> list = parser.parseLine(content);
        CompositeElement element = list.get(list.size()-1);
        list.forEach(repository::create);
        Assert.assertEquals(repository.read(list.size()-1),element);
    }
}
