package by.minsk.training.parser;

import by.minsk.training.controller.TextSource;
import by.minsk.training.model.CompositeElement;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;


public class ParserTest {

    private String content;

    @Before
    public void init() {
        content = new TextSource().read(new File(this.getClass().getClassLoader()
                .getResource("text.txt").getFile()).getAbsolutePath());
    }

    @Test
    public void shouldFindParagraphs() {
        ParserChain<CompositeElement> parser = new ParagraphParser();
        List<CompositeElement> list = parser.parseLine(content);
        Assert.assertTrue(list.size() > 1);
    }

    @Test
    public void shouldFindSentences() {
        ParserChain<CompositeElement> parser = new ParagraphParser();
        parser.linkWith(new SentenceParser());
        List<CompositeElement> list = parser.parseLine(content);
        System.out.println(list);
        List<CompositeElement> otherList = list.stream()
                .flatMap(p ->
                    p.getAll().stream()
                )
                .collect(Collectors.toList());
        Assert.assertTrue(otherList.size()-list.size()>0);
    }
}
