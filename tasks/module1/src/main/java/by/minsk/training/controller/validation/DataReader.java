package by.minsk.training.controller.validation;

import java.io.IOException;
import java.io.RandomAccessFile;
import static by.minsk.training.repository.ToyService.LOGGER;

public class DataReader {
    public String read(String path) {
        StringBuilder temp = new StringBuilder();
        try (RandomAccessFile file = new RandomAccessFile(path, "r")) {
            String line = null;
            while ((line = file.readLine()) != null) {
                temp.append(line);
            }
        } catch (IOException e) {
            LOGGER.error("Exception during reading a file " +e.getMessage());
        }
        return temp.toString();
    }
}
