package by.minsk.training.controller.build;

import by.minsk.training.entity.Colour;
import by.minsk.training.entity.Size;
import by.minsk.training.entity.Toy;
import by.minsk.training.entity.ToyCar;

import java.util.List;

public class ToyCarBuilder implements Builder {
    @Override
    public Toy build(List<String> list){
        return new ToyCar(Size.valueOf(list.get(1).toUpperCase()),
                Colour.valueOf(list.get(2).toUpperCase()),Double.valueOf(list.get(3)));
    }
}
