package by.minsk.training.entity;

public enum Colour {
    RED, WHITE, BLUE, GREEN, BLACK, GREY, VIOLET, PINK, YELLOW, ORANGE;

    public static boolean contains(String test) {
        for (Colour c : Colour.values()) {
            if (c.name().equals(test)) {
                return true;
            }
        }
        return false;
    }
}
