package by.minsk.training.controller.validation;

public interface Validator {
    ValidationResult validate (String input);
}
