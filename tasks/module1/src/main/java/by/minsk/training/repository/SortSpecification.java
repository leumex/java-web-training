package by.minsk.training.repository;

public interface SortSpecification<T> {

    int greaterThan(T t1, T t2);

    default SortSpecification<T> and (SortSpecification<T> other) {
        return (t3, t4) -> greaterThan(t3, t4) + other.greaterThan(t3, t4);
    }
}
