package by.minsk.training.repository;

public interface SearchSpecification<T> {
    boolean match(T entity);

    default SearchSpecification<T> and(SearchSpecification<T> other) {
        return entity -> match(entity) && other.match(entity);
    }
}
