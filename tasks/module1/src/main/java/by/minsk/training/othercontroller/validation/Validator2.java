package by.minsk.training.othercontroller.validation;

import by.minsk.training.othercontroller.validation.registration.ValidationOutput;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface Validator2 {
    ValidationOutput validate(String someString);

    default String getType(String someString) {
        List<String> interimList = Stream.of(someString.split(";"))
                .map(t -> t.toLowerCase())
                .filter(s -> s.contains("type"))
                .collect(Collectors.toList());
        if (interimList.size()>1) {
            return null;
        } else {
            String[] arr = interimList.get(0).split(":");
            return arr[1];
        }
    }
}
