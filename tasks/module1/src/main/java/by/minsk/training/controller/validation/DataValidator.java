package by.minsk.training.controller.validation;

import by.minsk.training.entity.Colour;
import by.minsk.training.entity.Room;
import by.minsk.training.entity.Size;
import by.minsk.training.util.NumberFormat;

import java.util.Arrays;
import java.util.List;

public class DataValidator implements Validator {

    private final List<String> toyTypes = Arrays.asList("BALL", "DOLL", "QUBE", "TOYCAR");

    @Override
    public ValidationResult validate(String content) {
        ValidationResult result = new ValidationResult();
        List<String> toys = Arrays.asList(content.split(";"));
        int count = 0;
        for (String s : toys) {
            List<String> parts = Arrays.asList(s.split(","));
            boolean marker = true;
            String[] subParts;
            if (parts.size() != 4) {
                result.addOmission(Omissions.INCORRECT_NUMBER_OF_ATTRIBUTES.toString(),
                        "at line " + toys.indexOf(s));
                marker = false;
            } else {
                subParts = parts.get(0).split(":");
                if (!toyTypes.contains(subParts[1].toUpperCase())) {
                    result.addOmission(Omissions.VALUE_OUTSIDE_POSSIBLE_RANGE.toString(),
                            "No such toy type, at line " + toys.indexOf(s));
                    marker = false;
                }
                subParts = parts.get(1).split(":");
                if (!Size.contains(subParts[1].toUpperCase())) {
                    result.addOmission(Omissions.VALUE_OUTSIDE_POSSIBLE_RANGE.toString(),
                            "No such toy size, at line " + toys.indexOf(s));
                    marker = false;
                }
                subParts = parts.get(2).split(":");
                if (!Colour.contains(subParts[1].toUpperCase())) {
                    result.addOmission(Omissions.VALUE_OUTSIDE_POSSIBLE_RANGE.toString(),
                            "No such toy colour , at line " + toys.indexOf(s));
                    marker = false;
                }
                subParts = parts.get(3).split(":");
                if (NumberFormat.isNumber(subParts[1])&&(Double.parseDouble(subParts[1]) > Room.COSTS_LIMIT)) {
                    result.addOmission(Omissions.VALUE_OUTSIDE_POSSIBLE_RANGE.toString(),
                            "Toy commercial value is beyond total toy budget!");
                    marker = false;
                }
            }
            if (marker) count++;
        }
        if (count > Room.QUANTITY_LIMIT) result.addOmission(Omissions.TOO_MANY_ITEMS.toString(),
                "Quantity of toys exceeds a limit!");
        return result;
    }
}
