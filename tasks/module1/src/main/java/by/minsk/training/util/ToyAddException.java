package by.minsk.training.util;

public class ToyAddException extends Exception {
    public ToyAddException(String message) {
        super(message);
    }
}
