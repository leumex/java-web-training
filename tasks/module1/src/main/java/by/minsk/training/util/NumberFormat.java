package by.minsk.training.util;

public class NumberFormat {
    public static boolean isNumber(String s) {
        char[] arr = s.toCharArray();
        if (!Character.isDigit(arr[0])) {
            return false;
        } else {
            boolean indicator = true;
            for (int i = 1; i < arr.length; i++) {
                if (!Character.isDigit(arr[i]) && (arr[i] == ',' || arr[i] == '.')) {
                    if (!indicator) {
                        return false;
                    } else {
                        indicator = false;
                    }
                } else if (!Character.isDigit(arr[i])) {
                    return false;
                }
            }
        }
        return true;
    }
}
