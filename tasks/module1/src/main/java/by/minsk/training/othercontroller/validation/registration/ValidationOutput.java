package by.minsk.training.othercontroller.validation.registration;

import java.util.List;
import java.util.Map;

public class ValidationOutput {
    private Map<String, List<String>> omissions;

    public boolean isValid(){
        return this.omissions.isEmpty();
    }
}
