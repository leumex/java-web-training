package by.minsk.training.entity;

public enum Size {
    SMALL, MEDIUM, BIG;

    public static boolean contains(String test) {
        for (Size c : Size.values()) {
            if (c.name().equals(test)) {
                return true;
            }
        }
        return false;
    }
}
