package by.minsk.training.repository;

import by.minsk.training.entity.Room;
import by.minsk.training.entity.Toy;
import by.minsk.training.util.ToyAddException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class ToyService implements Repository<Toy> {

    public static final Logger LOGGER = LogManager.getLogger();

    private Room room;

    public Room getRoom() {
        return room;
    }

    @Override
    public List<Toy> find(SearchSpecification<Toy> spec) {
        List<Toy> foundList = new ArrayList<>();
        for (Toy toy : room.getToys()) {
            if (spec.match(toy)) foundList.add(toy);
        }
        return foundList;
    }

    @Override
    public void sort(SortSpecification<Toy> spec) {
        room.getToys().sort( (Toy t1, Toy t2)-> spec.greaterThan(t1, t2));
    }

    @Override
    public void save(Toy toy) {
        if (room ==null) room = new Room();
        try {
            room.add(toy);
        } catch (ToyAddException e) {
            LOGGER.error("Room capacity limit exceeded!", e);
        }
    }
}
