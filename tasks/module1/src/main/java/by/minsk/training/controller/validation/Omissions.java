package by.minsk.training.controller.validation;

public enum Omissions {
    TOO_MANY_ITEMS, VALUE_OUTSIDE_POSSIBLE_RANGE, INCORRECT_NUMBER_OF_ATTRIBUTES
}
