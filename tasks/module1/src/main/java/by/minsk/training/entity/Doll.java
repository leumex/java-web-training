package by.minsk.training.entity;

public class Doll extends Toy {

    public Doll(Size size, Colour colour, double cost) {
        super(size, colour, cost);
    }

    @Override
    public String toString() {
        return "Doll " + super.toString();
    }
}
