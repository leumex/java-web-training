package by.minsk.training.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum ToyType {
    BALL, DOLL, QUBE, TOYCAR;

public static Optional<ToyType> fromString(String type){
        return Stream.of(ToyType.values()).filter(t->t.name().equalsIgnoreCase(type)).findFirst();
}
}
