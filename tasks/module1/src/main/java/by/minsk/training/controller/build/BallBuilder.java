package by.minsk.training.controller.build;

import by.minsk.training.controller.build.Builder;
import by.minsk.training.entity.*;

import java.util.List;

public class BallBuilder implements Builder {
    @Override
    public Toy build(List<String> list){
        return new Ball(Size.valueOf(list.get(1).toUpperCase()),
                Colour.valueOf(list.get(2).toUpperCase()),Double.valueOf(list.get(3)));
    }
}
