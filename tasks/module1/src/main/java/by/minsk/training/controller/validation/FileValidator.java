package by.minsk.training.controller.validation;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import static by.minsk.training.repository.ToyService.LOGGER;

public class FileValidator implements Validator {

    @Override
    public ValidationResult validate(String path) {
        ValidationResult result = new ValidationResult();
        try (FileReader fileReader = new FileReader(path)) {
            BufferedReader reader = new BufferedReader(fileReader);
            String testLine = reader.readLine();
            if (testLine == null){
                LOGGER.debug("File is empty!");
            result.addOmission("Nothing to read", "File is empty!");}
        } catch (FileNotFoundException exception) {
            LOGGER.error("File path is not valid!", exception);
            result.addOmission(exception.getClass().getName(), "invalid File path " + path);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            result.addOmission("Invalid content", "File is not available for reading/bytes ordering damage ");
        }
        return result;
    }
}
