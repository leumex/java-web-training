package by.minsk.training.controller.validation;

import by.minsk.training.controller.build.BallBuilder;
import by.minsk.training.controller.build.DollBuilder;
import by.minsk.training.controller.build.QubeBuilder;
import by.minsk.training.controller.build.ToyCarBuilder;
import by.minsk.training.entity.Toy;

import java.util.List;

public abstract class ToyFactory {

    public static Toy obtainToy(List<String> list) {
        Toy toy = null;
        switch (list.get(0).toLowerCase()) {
            case ("ball"): {
                toy = new BallBuilder().build(list);
                break;
            }
            case ("doll"): {
                toy = new DollBuilder().build(list);
                break;
            }
            case ("qube"): {
                toy = new QubeBuilder().build(list);
                break;
            }
            case ("toycar"): {
                toy = new ToyCarBuilder().build(list);
                break;
            }
            default: {
                break;
            }
        }
        return toy;
    }
}
