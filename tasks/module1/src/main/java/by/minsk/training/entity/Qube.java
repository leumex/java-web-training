package by.minsk.training.entity;

public class Qube extends Toy {
    public Qube(Size size, Colour colour, double cost) {
        super(size, colour, cost);
    }

    @Override
    public String toString() {
        return "Qube " + super.toString();
    }
}
