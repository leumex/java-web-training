package by.minsk.training.controller.validation;

import by.minsk.training.entity.Toy;
import by.minsk.training.repository.ToyService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class InputController {
    private DataValidator dataValidator;
    private FileValidator fileValidator;
    private DataReader reader;
    private LineParser parser;
    private ToyService service;

    public InputController(DataValidator dataValidator, FileValidator fileValidator,
                           DataReader reader, LineParser parser, ToyService service) {
        this.dataValidator = dataValidator;
        this.fileValidator = fileValidator;
        this.reader = reader;
        this.parser = parser;
        this.service = service;
    }

    private void restore(List<String> lines) {
        for (String s : lines) {
            List<String> list = parser.parse(s);
            Toy toy = ToyFactory.obtainToy(list);
            service.save(toy);
        }
    }

    public List<ValidationResult> updateToyList(String path) {
        List<ValidationResult> list = new ArrayList<>();
        list.add(fileValidator.validate(path));
        if (list.get(0).isValid()) {
            String content = reader.read(path);
            list.add(dataValidator.validate(content));
            List<String> subList = new ArrayList<>();
            subList.addAll(Arrays.asList(content.split(";")));
            if (list.get(1).isValid()) {
                restore(subList);
            } else {
                Set<Integer> damagedLines = list.get(1).getDamagedLines();
                if (!damagedLines.contains(-1)) {
                    for (int i : damagedLines) {
                        subList.remove(i);
                    }
                    restore(subList);
                }
            }
        }
        return list;
    }
}

