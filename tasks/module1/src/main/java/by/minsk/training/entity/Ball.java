package by.minsk.training.entity;

public class Ball extends Toy {
    public Ball(Size size, Colour colour, double cost) {
        super(size, colour, cost);
    }

    @Override
    public String toString() {
        return "Ball " + super.toString();
    }
}
