package by.minsk.training.controller.validation;

import java.util.*;

public class ValidationResult {
    private boolean isValid = true;
    private Map<String, List<String>> discrepancies = new HashMap<>();

    public void addOmission(String omissionName, String description) {
        if (discrepancies.containsKey(omissionName)) {
            discrepancies.get(omissionName).add(description);
        } else {
            List<String> descriptions = new ArrayList<>();
            descriptions.add(description);
            discrepancies.put(omissionName, descriptions);
        }
        isValid = false;
    }

    public boolean isValid() {
        return isValid;
    }

    public Set<Integer> getDamagedLines() {
        Set<Integer> damagedLines = new TreeSet<>();
        if (discrepancies.keySet().size() == 1 &&
                discrepancies.containsKey(Omissions.VALUE_OUTSIDE_POSSIBLE_RANGE.toString())) {
            for (String s : discrepancies.get(Omissions.VALUE_OUTSIDE_POSSIBLE_RANGE.toString())) {
                String[] temp = s.split("at line ");
                damagedLines.add(Integer.parseInt(temp[1].substring(0, 1)));
            }
        } else {
            damagedLines.add(-1);
        }
        return damagedLines;
    }

    public Map<String, List<String>> getDiscrepancies() {
        return discrepancies;
    }
}
