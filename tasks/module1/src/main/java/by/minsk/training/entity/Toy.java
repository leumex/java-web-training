package by.minsk.training.entity;

import java.util.Objects;

public class Toy {
    private Size size;
    private Colour colour;
    private double cost;

    public Toy(Size size, Colour colour, double cost) {
        this.size = size;
        this.colour = colour;
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Toy toy = (Toy) o;
        return Double.compare(toy.cost, cost) == 0 &&
                size == toy.size &&
                colour == toy.colour;
    }

    @Override
    public int hashCode() {
        return Objects.hash(size, colour, cost);
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public Colour getColour() {
        return colour;
    }

    public void setColour(Colour colour) {
        this.colour = colour;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return  this.getClass().getName()+
                "size=" + size +
                ", colour=" + colour +
                ", cost=" + cost +
                '}';
    }
}


