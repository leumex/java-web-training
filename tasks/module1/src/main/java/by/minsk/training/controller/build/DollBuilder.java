package by.minsk.training.controller.build;

import by.minsk.training.entity.Colour;
import by.minsk.training.entity.Doll;
import by.minsk.training.entity.Size;
import by.minsk.training.entity.Toy;

import java.util.List;

public class DollBuilder implements Builder {
    @Override
    public Toy build(List<String> list){
        return new Doll(Size.valueOf(list.get(1).toUpperCase()),
                Colour.valueOf(list.get(2).toUpperCase()),Double.valueOf(list.get(3)));
    }
}
