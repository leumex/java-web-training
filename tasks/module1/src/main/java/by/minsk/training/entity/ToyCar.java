package by.minsk.training.entity;

public class ToyCar extends Toy {
    public ToyCar(Size size, Colour colour, double cost) {
        super(size, colour, cost);
    }

    @Override
    public String toString() {
        return "ToyCar " + super.toString();
    }
}
