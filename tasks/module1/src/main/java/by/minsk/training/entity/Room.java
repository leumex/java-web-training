package by.minsk.training.entity;

import by.minsk.training.util.ToyAddException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Room {
    public static final int QUANTITY_LIMIT = 20;
    public static final double COSTS_LIMIT = 5000.00;
    private int totalQuantity;
    private double totalCosts;
    private List<Toy> toys;

    public Room() {
        this.toys = new ArrayList<>();
    }

    public void add(Toy toy) throws ToyAddException {
        if (totalCosts <= COSTS_LIMIT && totalQuantity <= QUANTITY_LIMIT) {
            totalCosts += toy.getCost();
            totalQuantity++;
            toys.add(toy);
        } else throw new ToyAddException("Room capacity is over");
    }

    public List<Toy> getToys() {
        return toys;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return Objects.equals(toys, room.toys);
    }

    @Override
    public int hashCode() {
        return Objects.hash(toys);
    }

    @Override
    public String toString() {
        return "Room{" +
                "toys=" + toys +
                '}';
    }
}
