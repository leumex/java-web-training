package by.minsk.training.repository;

import by.minsk.training.entity.Toy;

import java.util.List;

public interface Repository<T> {
    List<T> find(SearchSpecification<T> spec);
    void sort(SortSpecification<T> spec);
    void save(Toy toy);

}
