package by.minsk.training.controller.build;

import by.minsk.training.entity.Toy;

import java.util.List;

public interface Builder {
     Toy build(List<String> list);
}
