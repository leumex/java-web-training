package by.minsk.training.controller.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LineParser {
    public List<String> parse(String content) {
        List<String>list = Arrays.asList(content.split(","));
        List<String> resultList = new ArrayList<>();
        for(String s:list){
            String[] parts =s.split(":");
            resultList.add(parts[1]);
                    }
        return resultList;
    }
}
