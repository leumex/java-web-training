package by.minsk.training.service;

import by.minsk.training.entity.*;
import by.minsk.training.repository.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ToyServiceTest {

    private ToyService service = new ToyService();
    private static Logger logger = LogManager.getLogger(ToyServiceTest.class.getName());

    @Before
    public void init() {
                    service.save(new Qube(Size.BIG, Colour.BLUE, 123.25));
            service.save(new Ball(Size.MEDIUM, Colour.ORANGE, 23.54));
            service.save(new Doll(Size.SMALL, Colour.YELLOW, 67.64));
            service.save(new ToyCar(Size.MEDIUM, Colour.BLACK, 34.67));
            service.save(new ToyCar(Size.BIG, Colour.GREEN, 29.05));
            service.save(new Doll(Size.BIG, Colour.RED, 87.14));
            service.save(new Qube(Size.SMALL, Colour.WHITE, 18.69));
            service.save(new Ball(Size.SMALL, Colour.GREY, 65.71));
            service.save(new Qube(Size.MEDIUM, Colour.PINK, 43.54));
            service.save(new Doll(Size.BIG, Colour.WHITE, 71.87));
            service.save(new ToyCar(Size.MEDIUM, Colour.GREEN, 54.23));
            service.save(new Ball(Size.SMALL, Colour.VIOLET, 67.77));
            service.save(new Doll(Size.SMALL, Colour.ORANGE, 45.54));
            logger.info("Room has been equipped with toys");
    }

    @Test
    public void shouldFindToys() {
        ToySearchSpecification spec = new ToySearchSpecification();
        SearchSpecification<Toy> byTypeSpec = spec.and(new SearchSpecification<Toy>() {
            @Override
            public boolean match(Toy entity) {
                return entity.getClass().getName().equals("by.minsk.training.entity.Doll");
            }
        });
        SearchSpecification<Toy> byTypeAndSizeSpec = new SearchSpecification<Toy>() {
            @Override
            public boolean match(Toy entity) {
                return entity.getSize() == Size.SMALL;
            }
        }.and(byTypeSpec);
        List<Toy> foundSet = service.find(byTypeAndSizeSpec);

        Assert.assertNotNull(foundSet);
        Assert.assertEquals(2, foundSet.size());

    }

    @Test
    public void shouldSortToys() {
        List<Toy> beforeSortToys = new ArrayList<>();
        beforeSortToys.addAll(service.getRoom().getToys());
        ToySortSpecification spec = new ToySortSpecification();
        SortSpecification<Toy> byTypeSpec = spec.and(new SortSpecification<Toy>() {
            @Override
            public int greaterThan(Toy entity1, Toy entity2) {
                if (entity2.getClass().getName().compareTo(entity1.getClass().getName()) > 0) return 8;
                if (entity2.getClass().getName().compareTo(entity1.getClass().getName()) < 0) return -8;
                else return 0;
            }
        });
        SortSpecification<Toy> byTypeAndSizeSpec = new SortSpecification<Toy>() {
            @Override
            public int greaterThan(Toy entity1, Toy entity2) {
                if (entity2.getSize().compareTo(entity1.getSize()) > 0) return 4;
                if (entity2.getSize().compareTo(entity1.getSize()) < 0) return -4;
                else return 0;
            }
        }.and(byTypeSpec);
        service.sort(byTypeAndSizeSpec);
        List<Toy> afterSortToys = service.getRoom().getToys();
        Assert.assertNotEquals(beforeSortToys,afterSortToys);
    }
}
