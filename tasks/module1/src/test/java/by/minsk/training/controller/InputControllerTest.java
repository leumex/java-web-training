package by.minsk.training.controller;

import by.minsk.training.controller.validation.*;
import by.minsk.training.repository.ToyService;
import org.junit.Assert;
import org.junit.Test;
import java.util.List;

public class InputControllerTest {

    private List<ValidationResult> init(String path) {
        InputController controller = new InputController(new DataValidator(), new FileValidator(),
                new DataReader(), new LineParser(), new ToyService());
        return controller.updateToyList(path);
    }

    @Test
    public void shouldNotValidate() {
        List<ValidationResult> list = init(this.getClass().getResource("/datafile_badsubmitted1.txt").getPath());
        ValidationResult result = list.get(1);
        Assert.assertFalse(result.isValid());
    }

    @Test
    public void shouldGetCertainOmission() {
        List<ValidationResult> list = init(this.getClass().getResource("/datafile_badsubmitted2.txt").getPath());
        ValidationResult result = list.get(1);
        Assert.assertTrue(result.getDiscrepancies().containsKey(Omissions.INCORRECT_NUMBER_OF_ATTRIBUTES.toString()));
    }

    @Test
    public void shouldGenerateEmptyFileOmission() {
        List<ValidationResult> list = init(this.getClass().getResource("/data/datafile_empty.txt").getPath());
        ValidationResult result = list.get(0);
        List<String> descriptions = result.getDiscrepancies().get("Nothing to read");
        Assert.assertTrue(descriptions.contains("File is empty!"));
    }

    @Test
    public void shouldFailDueToLimitExcess() {
        List<ValidationResult> list = init(this.getClass().getResource("/datafile.txt").getPath());
        ValidationResult result = list.get(1);
        List<String> descriptions = result.getDiscrepancies().get(Omissions.TOO_MANY_ITEMS.toString());
        Assert.assertTrue(descriptions.contains("Quantity of toys exceeds a limit!"));
    }
}

