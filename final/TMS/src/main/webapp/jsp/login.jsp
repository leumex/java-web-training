<%--
  Created by IntelliJ IDEA.
  User: Asus
  Date: 27.11.2019
  Time: 0:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
   <%-- <link rel="stylesheet" href="static/styles.css"/>--%>
    <title>Login</title>
</head>
<body>
<h1>Authorization</h1>

<form action = "${pageContext.request.contextPath}/" method="post">
    <input type="hidden" name="commandName" value="loginUser">
    <input type="text" placeholder="userLogin" name="Login">
    <input type="text" placeholder="userPassword" name="Password">
    <button type="submit">login</button>
</form>

<a href="${pageContext.request.contextPath}/register">quick registration</a>

<c:if test="${requestScope.loginSuccess}">
    <c:out value="You have been logged into the system as ${sessionScope.user.name}"/>
</c:if>
<c:if test="${requestScope.notAuthorized!=null}">
    <c:out value="${requestScope.notAuthorized}"/>
</c:if>

</body>
</html>
