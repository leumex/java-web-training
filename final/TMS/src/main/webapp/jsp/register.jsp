<%--
  Created by IntelliJ IDEA.
  User: Asus
  Date: 28.11.2019
  Time: 7:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Registration</title>
</head>
<body>
<h1> Registration </h1>
<form action="${pageContext.request.contextPath}/" method="post">
    <input type="hidden" name="commandName" value="registerSaveUser">
    Name:
    <input type="text" placeholder="userName" name="name"><br>

    Role:<br>
    <input type="radio" name="role" value="customer">customer<br>
    <input type="radio" name="role" value="carrier">carrier<br>
    <input type="radio" name="role" value="admin">admin<br>

    Login:

    <input type="text" placeholder="userLogin" name="login"> <br>

    Password:

    <input type="text" placeholder="userPassword" name="password"><br>

    <button type="submit">register</button>

</form>

<c:if test="${requestScope.registerSuccess}">
    <c:out value=" Registration is successfully completed!"/>
</c:if>

<a href="${pageContext.request.contextPath}/">Back to main page</a>


</body>
</html>
