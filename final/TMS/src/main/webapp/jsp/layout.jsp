<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="by.minsk.training.user.UserEntity.Roles" %>
<%--<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/nav" %>--%>
<!DOCTYPE html>
<head>
    <title>TMS 2019(c)</title>
</head>
<body>
<h1>Transport Management System</h1>

<a href="${pageContext.request.contextPath}/login">Login</a>
<a href="${pageContext.request.contextPath}/logout">Logout</a>
<a href="${pageContext.request.contextPath}/register">Registration</a>
<a href="${pageContext.request.contextPath}/changeLang">Change language</a>

<c:if test="${requestScope.user.role eq Roles.customer &&
requestScope.user.role!=null}">
    <a href="${pageContext.request.contextPath}/tender">Tenders</a>
    <a href="${pageContext.request.contextPath}/tracking">Tracking</a>
    <a href="${pageContext.request.contextPath}/report">Reports</a>
    <a href="${pageContext.request.contextPath}/spot">Spot</a>
</c:if>

<c:if test="${requestScope.user.role eq Roles.transport_service_provider &&
requestScope.user.role!=null}">
    <a href="${pageContext.request.contextPath}/assignment">Assignment</a>
    <a href="${pageContext.request.contextPath}/tender">Tenders</a>
    <a href="${pageContext.request.contextPath}/tracking">Tracking</a>
    <a href="${pageContext.request.contextPath}/report">Reports</a>
</c:if>

<c:if test="${requestScope.user.role eq Roles.admin &&
requestScope.user.role!=null}">
    <a href="${pageContext.request.contextPath}/manage">Administration</a>
    <a href="${pageContext.request.contextPath}/report">Reports</a>
</c:if>

</body>
</html>
