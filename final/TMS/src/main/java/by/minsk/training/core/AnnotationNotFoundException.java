package by.minsk.training.core;

public class AnnotationNotFoundException extends BeanRegistrationException {

    public AnnotationNotFoundException(String message) {
        super(message);
    }

    public AnnotationNotFoundException(String message, Exception e) {
        super(message, e);
    }
}
