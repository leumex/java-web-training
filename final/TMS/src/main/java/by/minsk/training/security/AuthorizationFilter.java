package by.minsk.training.security;

import by.minsk.training.ApplicationContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/*")
public class AuthorizationFilter implements Filter {

    private static final Logger logger = LogManager.getLogger(AuthorizationFilter.class);
    private static final String[] freeFromLoginURIes = {"/", "/login", "/register"};

    private FilterConfig config;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        config = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        logger.debug("doFilter method has been invoked..");
        HttpServletRequest req = (HttpServletRequest) request;
        SecurityService service = ApplicationContext.getInstance().getBean(SecurityService.class);

        String path = req.getRequestURI().substring(req.getContextPath().length());
        logger.debug("incoming request has path: " + path);
        HttpSession session = req.getSession(false);
        boolean isLoggedIn = (session != null) && (session.getAttribute("user") != null);
        boolean isHomePage = path.equals(freeFromLoginURIes[0]);
        boolean isLogging = path.startsWith(freeFromLoginURIes[1]) || path.startsWith(freeFromLoginURIes[2]);

        if (isLoggedIn || isHomePage || isLogging) {
            chain.doFilter(request, response);
        } else {
            req.setAttribute("notAuthorized", "Please, log on or complete registration");
            req.getRequestDispatcher("/login").forward(req, (HttpServletResponse) response);
        }
    }

    @Override
    public void destroy() {
    }
}
