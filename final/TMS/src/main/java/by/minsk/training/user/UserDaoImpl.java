package by.minsk.training.user;

import by.minsk.training.dao.ConnectionManager;
import by.minsk.training.core.Bean;
import org.mindrot.jbcrypt.BCrypt;

import javax.servlet.ServletException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Bean
public class UserDaoImpl implements UserDao {

    private final static AtomicLong COUNTER = new AtomicLong(1);

    private static final String SELECT_ALL_QUERY = "";
    private static final String INSERT_QUERY = "insert into user(name, role, login, password, registration_date) values (?,?,?,?,?);";
    private static final String UPDATE_QUERY = "";
    private static final String DELETE_QUERY = "";
    private static final String SELECT_BY_ID_QUERY = "";
    private static final String SELECT_BY_LOGIN_QUERY = "select*from user where login = ?;";


    private ConnectionManager connectionManager;

    public UserDaoImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public Optional<UserDto> findByLogin(String login) throws SQLException {
        List<UserEntity> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_LOGIN_QUERY)) {
            selectStmt.setString(1,login);
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                UserEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        }
        return result.stream().map(this::fromEntity).findFirst();
    }

    @Override
    public Long save(UserDto userDto) throws SQLException {
        UserEntity entity = fromDTO(userDto);
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement insertStatement = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            insertStatement.setString(++i, entity.getName());
            insertStatement.setString(++i, entity.getRole().toString().toLowerCase());
            insertStatement.setString(++i, entity.getLogin());
            insertStatement.setString(++i, entity.getPassword());
            insertStatement.setDate(++i, Date.valueOf(LocalDate.now()));
            insertStatement.executeUpdate();
            ResultSet generatedKeys = insertStatement.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            }
        }
        return entity.getId();
    }

    @Override
    public boolean update(UserDto userDto) throws SQLException {
        UserEntity userEntity = fromDTO(userDto);
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(UPDATE_QUERY)) {
            int i = 0;
            updateStmt.setLong(++i, userEntity.getId());
            updateStmt.setString(++i, userEntity.getName());
            updateStmt.setString(++i, userEntity.getLogin());
            updateStmt.setString(++i, userEntity.getPassword());
            updateStmt.setString(++i, userEntity.getRole().toString());
            return updateStmt.executeUpdate() > 0;
        }
    }

    @Override
    public boolean delete(UserDto userDto) throws SQLException {
        UserEntity entity = fromDTO(userDto);
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement updateStmt = connection.prepareStatement(DELETE_QUERY)) {
            updateStmt.setLong(1, entity.getId());
            return updateStmt.executeUpdate() > 0;
        }
    }

    @Override
    public UserDto getById(Long id) throws SQLException {
        List<UserEntity> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_BY_ID_QUERY)) {
            selectStmt.setLong(1, id);
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                UserEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        }
        return result.stream().map(this::fromEntity).findFirst().orElseThrow(() -> new IllegalArgumentException("Entity not found with given id: " + id));

    }

    @Override
    public List<UserDto> findAll() throws SQLException {
        List<UserEntity> result = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement selectStmt = connection.prepareStatement(SELECT_ALL_QUERY)) {
            ResultSet resultSet = selectStmt.executeQuery();
            while (resultSet.next()) {
                UserEntity entity = parseResultSet(resultSet);
                result.add(entity);
            }
        }
        return result.stream().map(this::fromEntity).collect(Collectors.toList());
    }

    private UserEntity fromDTO(UserDto userDto) {
        UserEntity entity = new UserEntity();
        entity.setName(userDto.getName());
        entity.setRole(userDto.getRole());
        entity.setLogin(userDto.getLogin());
        entity.setPassword(userDto.getPassword());
        entity.setRegistrationDate(userDto.getRegistrationDate().toLocalDate());
        return entity;
    }

    private UserDto fromEntity(UserEntity entity) {
        UserDto dto = new UserDto();
        dto.setName(entity.getName());
        dto.setLogin(entity.getLogin());
        dto.setPassword(entity.getPassword());
        dto.setRole(entity.getRole());
        dto.setRegistrationDate(Date.valueOf(entity.getRegistrationDate()));
        return dto;
    }

    private UserEntity parseResultSet(ResultSet resultSet) throws SQLException{
        long entityId = resultSet.getLong("id");
        String login = resultSet.getString("login");
        String password = resultSet.getString("password");
        String name = resultSet.getString("name");
        UserEntity.Roles role = UserEntity.Roles.getRole(resultSet.getString("role").toLowerCase());
        LocalDate registrationDate = resultSet.getDate("registration_date").toLocalDate();
        return UserEntity.builder()
                .id(entityId)
                .login(login)
                .password(password)
                .name(name)
                .role(role)
                .registrationDate(registrationDate)
                .build();
    }
}
