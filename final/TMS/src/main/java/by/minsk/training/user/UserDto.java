package by.minsk.training.user;

import lombok.*;

import java.sql.Date;

@Data
@Builder
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    private String name;
    private String login;
    private String password;
    private UserEntity.Roles role;
    private Date registrationDate;

}
