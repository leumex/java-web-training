package by.minsk.training.user;

import by.minsk.training.core.Bean;
import by.minsk.training.core.TransactionalSupport;
import by.minsk.training.dao.Transactional;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Bean
@AllArgsConstructor
@TransactionalSupport
public class UserServiceImpl implements UserService {

    private static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

    private UserDao userDao;


    @Override
    public boolean loginUser(UserDto userDto) {
        Optional<UserDto> byLogin;
        try {
            byLogin = userDao.findByLogin(userDto.getLogin());
        } catch (SQLException e) {
            logger.error("Failed to read user", e);
            byLogin = Optional.empty();
        }
        return byLogin.filter(dto -> dto.getPassword().equals(userDto.getPassword())).isPresent();
    }


    @Override
    @Transactional
    public boolean registerUser(UserDto userDto) {
        try {
            Long saved = userDao.save(userDto);
            return true;
        } catch (SQLException e) {
            logger.error("Failed to save user", e);
            return false;
        }
    }

    @Override
    public List<UserDto> getAllUsers() {
        try {
            return userDao.findAll();
        } catch (SQLException e) {
            logger.error("Failed to read users", e);
            return new ArrayList<>();
        }
    }

    @Override
    public UserDto findUser(String login) {
        try {
            Optional<UserDto> result = userDao.findByLogin(login);
             return (result.orElse(null));
        } catch (SQLException e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }
}
