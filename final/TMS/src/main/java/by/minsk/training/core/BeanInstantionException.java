package by.minsk.training.core;

public class BeanInstantionException extends BeanRegistrationException{
    public BeanInstantionException(String s) {
        super(s);
    }
    public BeanInstantionException (String message, Exception e){
        super(message, e);
    }
}
