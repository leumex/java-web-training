package by.minsk.training.user;

import java.util.List;

public interface UserService {

    boolean loginUser(UserDto userDto);

    boolean registerUser(UserDto userDto);

    UserDto findUser(String login);

    List<UserDto> getAllUsers();
}
