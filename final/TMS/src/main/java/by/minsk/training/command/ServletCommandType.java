package by.minsk.training.command;

import by.minsk.training.user.UserEntity;

public enum ServletCommandType {

    DEFAULT("default", UserEntity.Roles.all),
    LOGIN("loginUser", UserEntity.Roles.all),
    LOGOUT("logoutUser", UserEntity.Roles.all),
    VIEW_USER_LIST("viewUserList", UserEntity.Roles.admin),
    REGISTER("registerUser", UserEntity.Roles.all),
    TENDER_RELEASE(" tenderRelease", UserEntity.Roles.customer),
    TENDER_PARTICIPATE("tenderParticipate", UserEntity.Roles.transport_service_provider),
    TRANSPORT_ORDER_RELEASE("transportOrderRelease", UserEntity.Roles.customer),
    TRANSPORT_ORDER_ASSIGN("transportOrderAssign", UserEntity.Roles.customer),
    TRANSPORT_ORDER_EXECUTION("transportOrderExecution", UserEntity.Roles.all),
    INVOICE_ISSUE("invoiceIssue", UserEntity.Roles.transport_service_provider),
    INVOICE_PAY("invoicePay",UserEntity.Roles.customer),
    VIEW_TRANSPORT_ORDER_LIST ("viewTransportOrderList", UserEntity.Roles.all),
    VIEW_INVOICE_LIST("viewInvoiceList", UserEntity.Roles.all),
    VIEW_TENDER_LIST("viewTenderList", UserEntity.Roles.all);


    private String name;
    private UserEntity.Roles role;

    ServletCommandType(String name, UserEntity.Roles role) {
        this.name = name;
        this.role = role;
    }
}
