package by.minsk.training.command;

import by.minsk.training.core.Bean;
import by.minsk.training.user.UserDto;
import by.minsk.training.user.UserEntity;
import by.minsk.training.user.UserService;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;

import static by.minsk.training.ApplicationConstants.REG_NEW_USER;

@Bean(name = REG_NEW_USER)
@AllArgsConstructor
public class RegisterUserSaveCommand implements ServletCommand {
    private static final Logger logger = LogManager.getLogger(RegisterUserSaveCommand.class);

    private UserService userService;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        logger.debug("from within RegisterSave command");
        UserDto userDto = new UserDto();
        UserEntity.Roles role;
        try {
            String name = (String) req.getParameter("name");
            switch (((String) req.getParameter("role")).trim().toLowerCase()) {
                case ("customer"): {
                    role = UserEntity.Roles.customer;
                    break;
                }
                case ("carrier"): {
                    role = UserEntity.Roles.transport_service_provider;
                    break;
                }
                case ("admin"): {
                    role = UserEntity.Roles.admin;
                    break;
                }
                default: {
                    role = null;
                    break;
                }
            }
            if (role == null) {
                throw new ServletException("New user hasn't defined his role");
            }
            String login = (String) req.getParameter("login");
            String password = (String) req.getParameter("password");
            String hashPassword = BCrypt.hashpw(password, BCrypt.gensalt());
            Date registrationDate = Date.valueOf(LocalDate.now());
            logger.debug(name + ", " + role + ", " + login + ", " + hashPassword + " user details prepared for insert on " + registrationDate);
            userDto.setLogin(login);
            userDto.setName(name);
            userDto.setPassword(hashPassword);
            userDto.setRole(role);
            userDto.setRegistrationDate(registrationDate);
            final boolean saved = userService.registerUser(userDto);
            if (saved) {
                req.setAttribute("registerSuccess", true);
                req.getRequestDispatcher("/register").forward(req,resp);
            } else req.setAttribute ("registerSuccess", false);
            req.getRequestDispatcher("/register").forward(req,resp);
        } catch (ServletException | IOException e) {
            throw new CommandException("failed to register user", e);
        }
    }
}

