package by.minsk.training.core;

public class BeanRegistrationException extends RuntimeException {

    public BeanRegistrationException(String message, Exception e) {
        super(message, e);
    }

    public BeanRegistrationException(String message) {
        super(message);
    }
}
