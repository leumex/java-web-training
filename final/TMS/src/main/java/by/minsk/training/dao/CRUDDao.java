package by.minsk.training.dao;

import java.sql.SQLException;
import java.util.List;

public interface CRUDDao<E, K> {

    K save(E entity) throws SQLException;

    boolean update(E entity) throws SQLException;

    boolean delete(E entity) throws SQLException;

    E getById(K id) throws SQLException;

    List<E> findAll() throws SQLException;

}
