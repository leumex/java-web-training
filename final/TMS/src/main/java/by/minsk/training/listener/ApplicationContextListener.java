package by.minsk.training.listener;

import by.minsk.training.ApplicationContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


public class ApplicationContextListener implements ServletContextListener {

    private final static Logger logger = LogManager.getLogger(ApplicationContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce){
        logger.info(this.getClass().getName() + " tries to initialize Application context...");
        ApplicationContext.initialize();
        logger.info("Context has been initialized");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce){
        ApplicationContext.getInstance().destroy();
        logger.info("Context has been destroyed");
    }
}
