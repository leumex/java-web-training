package by.minsk.training.security;

public class AuthorizedException extends Exception {
    public AuthorizedException(String message){
        super(message);
    }

    public AuthorizedException(String message, Exception e){
        super(message, e);
    }
}
