package by.minsk.training.user;

import by.minsk.training.dao.CRUDDao;

import java.sql.SQLException;
import java.util.Optional;

public interface UserDao extends CRUDDao<UserDto, Long> {

Optional<UserDto> findByLogin (String login) throws SQLException;
}
