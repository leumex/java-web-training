package by.minsk.training.core;

public class RepeatedBeanException extends BeanRegistrationException {
    public RepeatedBeanException(String message) {
        super(message);
    }
}
