package by.minsk.training.command;

import by.minsk.training.ApplicationContext;
import by.minsk.training.core.Bean;
import by.minsk.training.security.AuthorizedException;
import by.minsk.training.security.SecurityService;
import by.minsk.training.user.UserEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static by.minsk.training.ApplicationConstants.LOGIN_USER;

@Bean(name = LOGIN_USER)
public class LoginUserCommand implements ServletCommand {

    private static final Logger logger = LogManager.getLogger(LoginUserCommand.class);


    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        logger.debug("LoginUser command has been invoked");
        String login = req.getParameter("Login");
        String password = req.getParameter("Password");
        SecurityService service = ApplicationContext.getInstance().getBean(SecurityService.class);
        boolean loggedOn;
        try {
            loggedOn = service.logIn(req, login, password);
            if (loggedOn) {
                logger.debug("successful logging");
                req.setAttribute("loginSuccess", true);
                req.getRequestDispatcher("/login").forward(req, resp);
            }
        } catch (AuthorizedException | ServletException | IOException e) {
            logger.error(e.getMessage());
            throw new CommandException(e.getMessage(), e);
        }
    }
}
