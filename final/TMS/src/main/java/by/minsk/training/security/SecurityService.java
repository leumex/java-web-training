package by.minsk.training.security;

import by.minsk.training.ApplicationContext;
import by.minsk.training.core.Bean;
import by.minsk.training.user.UserDto;
import by.minsk.training.user.UserService;
import by.minsk.training.user.UserServiceImpl;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Bean
@NoArgsConstructor
public class SecurityService {

    private static final Logger logger = LogManager.getLogger(SecurityService.class);
    private Map<String, UserDto> authorizedUsers = new ConcurrentHashMap<>();

    public boolean logIn(HttpServletRequest req, String login, String password) throws AuthorizedException {
        UserDto userDto = new UserDto();
        userDto.setLogin(login);
        String hashPw = BCrypt.hashpw(password, BCrypt.gensalt());
        logger.debug("encrypted password is "+ hashPw);
        userDto.setPassword(hashPw);
        UserService userService = ApplicationContext.getInstance().getBean(UserServiceImpl.class);
        boolean loggedIn = userService.loginUser(userDto);
        if (loggedIn && !authorizedUsers.containsValue(userDto)) {
            authorizedUsers.put(req.getSession().getId(), userDto);
            userDto = userService.findUser(login);
            req.getSession().setAttribute("user", userDto);
        } else if (loggedIn) {
            throw new AuthorizedException("Someone has already logged in with these credentials!");
        }
        return loggedIn;
    }

    public void logOut(HttpServletRequest req) throws AuthorizedException {
        HttpSession session = req.getSession(false);
        if (session != null) {
            session.removeAttribute("user");
            authorizedUsers.remove(session.getId());
            session.invalidate();
        } else {
            throw new AuthorizedException("No alive session accompanying the request");
        }
    }

    public UserDto getCurrentUser(HttpServletRequest req) throws AuthorizedException {
        UserDto user;
        try {
            String id = req.getSession().getId();
            user = authorizedUsers.get(id);
        } catch (IllegalStateException e) {
            throw new AuthorizedException("No alive session accompanying the request", e);
        }
        return user;
    }
}
