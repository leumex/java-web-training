package by.minsk.training.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.NoSuchElementException;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity {
    private Long id;
    private String login;
    private String name;
    private UserEntity.Roles role;
    private String password;
    private LocalDate registrationDate;

    public enum Roles {
        customer("customer"), transport_service_provider("transport service provider"), admin("admin"), all("all");

        String representation;

        Roles(String representation) {
            this.representation = representation;
        }

        static Roles getRole(String name) throws SQLException {
            Roles definedValue;
            try {
                definedValue = Arrays.stream(Roles.values()).filter(role -> role.toString().equals(name)).findFirst().get();
            } catch (NoSuchElementException e) {
                throw new SQLException("Unknown role", e);
            }
            return definedValue;
        }

        @Override
        public String toString() {
            return this.representation;
        }
    }
}
